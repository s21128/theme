

        <!-- Footer -->
        <footer>
            <div class="row well">
                
                
                    <div class="col-md-8">                 
                        <p>Copyright &copy; Patryk Piosik 2017</p>

                    </div>
                    <div class="col-md-4">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
                        <?php endif; ?>
                    </div>
      
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->


    <!-- jQuery -->
    <!-- <script src="bootstrap/js/jquery.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <!-- <script src="bootstrap/js/bootstrap.min.js"></script> -->
    
<?php wp_footer(); ?>
</body>
</html>
