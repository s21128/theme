 
<?php get_header(); ?>


        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="row">   
            <div class="panel panel-success">
                <div class="panel-heading text-center"><h3><?php the_title(); ?></h3></div>
                    <div class="panel-body">
            <div class="col-md-6">               
                <?php if(has_post_thumbnail()): ?>
                    <a href="<?php the_post_thumbnail_url(); ?>" title="<?php the_title_attribute(); ?>">                   
                       <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive responsive--full', 'title' => the_title_attribute() ]); ?>
                    </a>
                 <?php else: ?>
                 <img class="img-responsive" src="http://placehold.it/600x300" alt="">
                 <?php endif; ?>
                <h6><span class="help-block"><?php the_category(' '); ?></span></h6>
            </div>
            <div class="col-md-6">
                <h4><?php the_author(); ?></h4>
                <h6><?php the_time('l, F jS, Y'); ?></h6>
                <p><?php the_content(); ?></p>                
                <a class="btn btn-primary" href="<?php the_permalink(); ?>">Zobacz post <span class="glyphicon glyphicon-chevron-right"></span></a>                
            </div>
                    </div>
            </div>
        </div>   <!-- /.row -->
           <hr>
            <?php endwhile; else: ?>
                    <p><?php _e('Brak postów'); ?></p>
            <?php endif; ?>

     
        
        <?php get_footer(); ?>