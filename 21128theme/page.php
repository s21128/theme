<?php get_header(); ?>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="row well">
            <div class="col-md-12">
           
                <h3><?php the_title(); ?></h3>
                <p><?php the_content(); ?></p>     
           
            </div>
        </div>   <!-- /.row -->

        <?php endwhile; else: ?>
                <p><?php _e('Brak danych'); ?></p>
        <?php endif; ?>   
                
        <?php get_footer(); ?>
